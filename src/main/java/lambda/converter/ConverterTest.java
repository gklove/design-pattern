package lambda.converter;

public class ConverterTest {

    public static void main(String[] args) {

//        Converter<String, Integer> converter = (from )->Integer.valueOf(from);
        Converter<String, Integer> converter = Integer::valueOf;
        Integer result = converter.converter("123");
        System.out.println(result);
    }
}
