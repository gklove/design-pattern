package lambda.converter;

public interface Converter<F, T> {
    T converter(F from);
}
