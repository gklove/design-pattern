package lambda.person;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class PersonTest {
    public static void main(String[] args) {
        System.out.println("===============1=====================");
        PersonFactory<Person> factory = Person::new;
        Person p = factory.create("1", "张三");
        System.out.println(p.toString());

        System.out.println("===============2=====================");
        //Supplier接口产生一个给定类型的结果
        Supplier<Person> personSupplier = Person::new;
        Person person = personSupplier.get();
        System.out.println(person);

        System.out.println("===============3=====================");
        //Consumer代表了在一个输入参数上需要进行的操作。
        Consumer<Person> greeter = (cp) -> System.out.println("Hello, " + cp.getName());
        greeter.accept(new Person("1", "李四"));

        System.out.println("===============4=====================");
        Comparator<Person> personComparator = (p1, p2) -> p1.getId().compareTo(p2.getId());
        Person a = new Person("1", "赵云");
        Person b = new Person("2", "马超");
        int result = personComparator.compare(a, b);
        int reverseResult = personComparator.reversed().compare(a, b);
        System.out.println(result);
        System.out.println(reverseResult);

        System.out.println("===============5=====================");
        Optional<String> optional = Optional.of("bag");
        System.out.println(optional.isPresent());
        System.out.println(optional.orElse("pig"));
        System.out.println(optional.get());
        optional.ifPresent(o-> System.out.println(o.charAt(1)));
    }
}
