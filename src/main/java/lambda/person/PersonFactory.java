package lambda.person;

 interface PersonFactory<T extends Person> {
     T create(String id, String name);
}
