package algorithm;

public class SparseArray {
    public static void main(String[] args) {
        //创建一个二维数组 11*11
        //0 表示没有棋子，  1表示黑子  2 表示蓝子
        int chessArr[][] = new int[11][11];
        chessArr[1][2] = 1;
        chessArr[2][3] = 2;
        chessArr[2][4] = 9;
        System.out.println("原始的二维数组");
        for (int[] row : chessArr) {
            for (int data : row) {
                System.out.printf("%d\t", data);
            }
            System.out.println();
        }
        System.out.println();

        //将二维数组 转换成稀疏数组
        //1.先遍历二维数组  得到非0数据个数
        int sum = 0;
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if (chessArr[i][j] != 0) {
                    sum++;
                }
            }
        }
        //创建稀疏数组
        int sparseArr[][] = new int[sum + 1][3];
        //给稀疏数组赋值
        sparseArr[0][0] = 11;
        sparseArr[0][1] = 11;
        sparseArr[0][2] = sum;
        //遍历稀疏数组 ，将非0的值放入 sparseArr
        int index = 0;//用来记录是第几个非0的数据
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if (chessArr[i][j] != 0) {
                    index++;
                    sparseArr[index][0]=i;//第几行
                    sparseArr[index][1]=j;//第几列
                    sparseArr[index][2]=chessArr[i][j];//具体值
                }
            }
        }
        System.out.println();
        System.out.println("得到的稀疏数组是.........");
        for (int i = 0; i < sparseArr.length; i++) {
            System.out.printf("%d\t%d\t%d\t\n",sparseArr[i][0],sparseArr[i][1],sparseArr[i][2]);
        }
        System.out.println();


        System.out.println("原先的二维数组是：");
        for (int i = 0; i < sparseArr[0][0]; i++) {
            for (int j = 0; j < sparseArr[0][1]; j++) {
                for (int h = 1; h <= sparseArr[0][2]; h++) {
                    chessArr[h][sparseArr[h][1]] = sparseArr[h][2];
                }
            }
        }
        System.out.println();
        for (int[] row : chessArr) {
            for (int data : row) {
                System.out.printf("%d\t",data);

            }
            System.out.println();
        }
        System.out.println();
    }



}
