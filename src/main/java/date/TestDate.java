package date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class TestDate {
    public static void main(String[] args) {

        LocalDateTime now = LocalDateTime.now();

        String s1 = now.format(DateTimeFormatter.ISO_DATE);
        String s2 = now.format(DateTimeFormatter.ISO_DATE_TIME);
        String s3 = now.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
        String s4 = now.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG));
        String s5 = now.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        String s6 = now.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        String s7 = now.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS"));

        System.out.println("ISO_DATE:  " + s1);
        System.out.println("ISO_DATE_TIME:  " + s2);
        System.out.println("FULL:  " + s3);
        System.out.println("LONG:  " + s4);
        System.out.println("MEDIUM:  " + s5);
        System.out.println("SHORT:  " + s6);
        System.out.println("yyyy/MM/dd HH:mm:ss:SSS:  " + s7);

        LocalDateTime time = LocalDateTime.parse(s2);
        System.out.println(time);

    }
}
