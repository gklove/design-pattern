package time;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MyTimer {
    public static void main(String[] args) throws ParseException {

        LocalDateTime start = LocalDateTime.now();
        String startStr = start.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS"));

        List<Integer> count = new ArrayList<>();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                count.add(1);
                System.out.println("aaa");
                if (count.size() == 12) {//控制次数
                    timer.cancel();
                    LocalDateTime end = LocalDateTime.now();
                    String endStr = end.format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS"));
                    long diff = 0;
                    try {
                         diff = getTime(startStr, endStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    System.out.println("耗时"+diff);
                }
            }
        },0,5000);//启动后，每隔10s运行一次 总计1分钟
//        compute(1,2);

    }
    public static long getTime(String oldTime,String newTime) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
        long NTime =df.parse(newTime).getTime();
        //从对象中拿到时间
        long OTime = df.parse(oldTime).getTime();
        long diff=(NTime-OTime);
        return diff;
    }

    public static void compute(int molecular,int denominator) {
        // 创建一个数值格式化对象
        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置精确到小数点后2位
        numberFormat.setMaximumFractionDigits(2);
        String result = numberFormat.format((float) molecular / (float) denominator * 100);
        System.out.println("num1和num2的百分比为:" + result + "%");
    }
}
