package designmodel.六大设计原则.迪米特法则;

import java.util.ArrayList;
import java.util.List;

/**
 * 定义:一个对象应该对其他对象有最少的了解。只与直接的朋友通信。
 *
 * 朋友关系：每个对象都必然会与其他对象有耦合关系，两个对象之间的耦合就称为朋友关系。
 *
 * 我的理解：迪米特法则定义的是类之间的关系要尽量低耦合，一个类中的朋友不要太多，
 *
 * 这样在后期代码维护或更改时需要修改的地方也就不会太多了。
 */
public class ClassTest {
    public static void main(String[] args) {
        List<Girl> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(new Girl());
        }
        new Teacher().command(new ClassMonitor(list));
    }
}
