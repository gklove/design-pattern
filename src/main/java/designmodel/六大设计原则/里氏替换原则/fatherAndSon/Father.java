package designmodel.六大设计原则.里氏替换原则.fatherAndSon;

import java.util.Collection;
import java.util.HashMap;

/**
 * 定义一个父类，实现将map集合转换成Collection集合
 * @author admin
 *
 */
class Father{
	public Collection doSomething(HashMap<String,String> map){
		System.out.println("父类方法被执行...");
		return map.values();
	}
}