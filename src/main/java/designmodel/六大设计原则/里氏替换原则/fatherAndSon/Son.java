package designmodel.六大设计原则.里氏替换原则.fatherAndSon;

import java.util.Collection;
import java.util.Map;

/**
 * 子类重载父类的方法
 * @author admin
 *
 */
class Son extends Father{
	/**
	 * 注意此处是重载，返回值类型，方法名相同，传入参数不同。
	 * 保证传入的参数类型的范围大于父类。
	 */
	public Collection doSomething(Map<String, String> map) { //如果此处是map，则放大，hashMap则是缩小
		System.out.println("子类方法被执行...");
		return map.values();
	}
}