package designmodel.六大设计原则.里氏替换原则.fatherAndSon;

import java.util.HashMap;

public class Test02 {
	public static void main(String[] args) {
		//覆写或实现父类的方法时输出结果可以被放大   或者缩小
		Father f = new Father();
		HashMap<String, String> hashMap = new HashMap<String, String>();
		f.doSomething(hashMap);
		System.out.println("里氏替换----------------父类存在的地方子类应该也可以存在-----------");
		Son s = new Son();
		s.doSomething(hashMap);
    }
}