package designmodel.六大设计原则.里氏替换原则;

//狙击手
public class Snipper {
    private Aug aug;

    public void setGun(Aug gun) {
        this.aug = gun;
    }

    public void killEnemy() {
        aug.zoomOut();
        aug.shoot();
    }
}
