package designmodel.六大设计原则.里氏替换原则;

/**
 * 定义：所有引用基类的地方必须能够透明地使用其子类的对象
 * 我的理解： 父类出现的地方也可以用子类来替换，而子类出现的地方父类不一定能替换。
 *
 * 里氏替换原则的为继承定义的规范，包括4层含义
 *
 * 1、子类必须完全实现父类的方法
 *
 * 2、子类可以有自己的个性
 *
 * 3、覆盖或实现父类的方法时输入参数可以被放大
 *
 * 4、覆写或实现父类的方法时输出结果可以被缩小
 */
public class GunTest {
    public static void main(String[] args) {
        Solider solider = new Solider();
        solider.setGun(new MachineGun());
        solider.killEnemy();

//        子类可以有自己的个性
        Snipper jujishou = new Snipper();
        jujishou.setGun(new Aug());
        jujishou.killEnemy();
    }
}
