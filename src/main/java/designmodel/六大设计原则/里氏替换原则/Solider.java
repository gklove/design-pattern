package designmodel.六大设计原则.里氏替换原则;

public class Solider {
    private AbstractGun gun;

    public void setGun(AbstractGun gun) {
        this.gun = gun;
    }

    public void killEnemy() {
        System.out.println("正在射杀敌人...");
        gun.shoot();

    }
}
