package designmodel.六大设计原则.依赖倒置原则;

/**
 * 低层模块：具体细化的java类
 *
 * 高层模块：是由多个低层模块组成的。
 *
 * 抽象：指的是接口或者抽象类
 *
 * 依赖：存在类A的一个方法S,S传入的参数是另一个类B的实例，那么类A依赖于类B
 *
 * 我的理解---在项目开发中尽量将相同模块细化，类与类之间减少依赖关系，应该依赖其抽象类或者接口。
 */
public class CarTest {
    public static void main(String[] args) {
        Driver driver = new Driver();
        driver.drive(new BenzCar());
    }
}
