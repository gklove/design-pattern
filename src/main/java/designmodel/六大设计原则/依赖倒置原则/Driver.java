package designmodel.六大设计原则.依赖倒置原则;

public class Driver implements IDriver {
    @Override
    public void drive(ICar car) {
        System.out.println("司机在开车....");
        car.run();

    }
}
