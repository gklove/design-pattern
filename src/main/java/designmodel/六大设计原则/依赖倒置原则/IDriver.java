package designmodel.六大设计原则.依赖倒置原则;

public interface IDriver {
     void drive(ICar car);
}
