package designmodel.六大设计原则.开闭原则;

public interface IBook {
    int getPrice();

    String getName();

    String getAuthor();

}
