package designmodel.六大设计原则.开闭原则;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class BookStore {
    public static List<OffNovelBooks> offlistBooks = new ArrayList<>();
    public static List<NovelBooks> listBooks = new ArrayList<>();
    static {
        listBooks.add(new NovelBooks("三重门","韩寒",3000));
        listBooks.add(new NovelBooks("人生","路遥",2400));
        listBooks.add(new NovelBooks("荆棘鸟","考琳·麦卡洛",5000));
        listBooks.add(new NovelBooks("从你的全世界路过","张嘉佳",2500));

        offlistBooks.add(new OffNovelBooks("三重门","韩寒",3000));
        offlistBooks.add(new OffNovelBooks("人生","路遥",2400));
        offlistBooks.add(new OffNovelBooks("荆棘鸟","考琳·麦卡洛",5000));
        offlistBooks.add(new OffNovelBooks("从你的全世界路过","张嘉佳",2500));
    }

    public static void main(String[] args) {
        //返回默认的货币格式
        NumberFormat formatter  = NumberFormat.getCurrencyInstance();

        System.out.println("打折活动开始前.....");
        for (NovelBooks book : listBooks) {
            System.out.println("书籍名称："+book.getName()+"\t书籍作者："+book.getAuthor()+"\t书籍价格："+formatter
                    .format(book.getPrice()/100.00)+"元");
        }

        System.out.println("打折活动开始后.....");

        for (OffNovelBooks book : offlistBooks) {
            System.out.println("书籍名称："+book.getName()+"\t书籍作者："+book.getAuthor()+"\t书籍价格："+formatter
                    .format(book.getPrice()/100.00)+"元");
        }
    }
}
