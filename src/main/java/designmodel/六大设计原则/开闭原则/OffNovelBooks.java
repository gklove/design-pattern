package designmodel.六大设计原则.开闭原则;

public class OffNovelBooks extends NovelBooks {
    public OffNovelBooks(String name, String author, int price) {
        super(name, author, price);
    }

    @Override
    public int getPrice() {
        int oldPrice = super.getPrice();
        int newPice = 0;
        if (oldPrice > 4000) {
            newPice = oldPrice * 80 / 100;//打八折
        } else {
            newPice = oldPrice * 90 / 100;
        }
        return newPice;
    }
}
