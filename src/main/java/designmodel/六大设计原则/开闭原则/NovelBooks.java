package designmodel.六大设计原则.开闭原则;

public class NovelBooks implements IBook {
    private int price;
    private String name;
    private String author;

    public NovelBooks( String name, String author,int price) {
        this.price = price;
        this.name = name;
        this.author = author;
    }

    @Override
    public int getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }
}
