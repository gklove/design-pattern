package designmodel.六大设计原则.接口隔离原则;

public class YYGirl implements IGoodBodyGirl,ITemperamentPettyGirl {
    private String name;

    public YYGirl(String name) {
        this.name = name;
    }

    @Override
    public void goodLook() {
        System.out.println(this.name+"拥有好看的外表");
    }

    @Override
    public void niceFigure() {
        System.out.println(this.name+"拥有完美的身材");

    }

    @Override
    public void greatTemperament() {
        System.out.println(this.name+"拥有超好的脾气");
    }
}
