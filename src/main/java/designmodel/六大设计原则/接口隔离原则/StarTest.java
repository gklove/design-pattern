package designmodel.六大设计原则.接口隔离原则;

/**
 * 接口隔离原则是对接口进行的规范约束，有以下四层含义（摘自《设计模式之禅》）
 *
 * 1、接口要尽量小
 *
 * 接口在遵守单一职责原则的基础下，尽可能细化接口的方法。并不是无休止的细化接口。
 *
 * 2、接口要高内聚
 *
 * 高内聚是提高接口、类、模块的处理能力，减少对外的交互。在接口中尽量少公布public方法，接口是对外的承诺，承诺越少对外越有利，变更的风险也就越少，同时有利于降低成本
 *
 * 3、定制服务
 *
 * 根据需求分析，必要时为特殊用户提供定制的接口，尽量避免不同操作人群使用同一接口，这样会降低系统的响应速度和扩展性。
 *
 * 4、接口设计是有限度的
 *
 * 根据开发情景来划分设计接口，在开发过程中设计接口的颗粒度越小，系统灵活性越高。并不是以为的将接口细化。
 */
public class StarTest {
    public static void main(String[] args) {
        StarSearch starSearch = new StarSearch("王导");
        starSearch.show();
        starSearch.AbstractSearch((IGoodBodyGirl)new YYGirl("张慧雯"));
        starSearch.AbstractSearch((ITemperamentPettyGirl)new YYGirl("张慧雯"));
    }
}
