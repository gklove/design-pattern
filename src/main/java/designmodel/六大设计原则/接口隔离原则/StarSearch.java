package designmodel.六大设计原则.接口隔离原则;

public class StarSearch extends AbstractSearcher {

    private String name;

    public StarSearch(String name) {
        this.name = name;
    }

    @Override
    public void AbstractSearch(IGoodBodyGirl bodyGirl) {
        bodyGirl.niceFigure();
        bodyGirl.goodLook();
    }

    @Override
    public void AbstractSearch(ITemperamentPettyGirl temperamentPettyGirl) {
        temperamentPettyGirl.greatTemperament();
    }

    @Override
    public void show() {
        System.out.println("星探"+this.name+"发现美女");
    }
}
