package designmodel.六大设计原则.接口隔离原则;

abstract class AbstractSearcher {
    public abstract void AbstractSearch(IGoodBodyGirl bodyGirl);

    public abstract void AbstractSearch(ITemperamentPettyGirl bodyGirl);

    public abstract void show();
}
