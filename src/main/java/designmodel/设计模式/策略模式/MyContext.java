package designmodel.设计模式.策略模式;


/**
 * 具体的承载妙计的锦囊。
 * @author admin
 *
 */
public class MyContext {
    //一个私有对象指向具体的实现算法类。
    private IStrategy strategy;

    public MyContext(IStrategy strategy) {
        this.strategy = strategy;
    }

    public void operate() {
        this.strategy.operate();
    }
}
