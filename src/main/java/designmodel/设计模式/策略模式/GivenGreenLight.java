package designmodel.设计模式.策略模式;

/**
 * 锦囊妙计二
 * @author admin
 *
 */
class GivenGreenLight implements IStrategy{
	@Override
	public void operate() {
		System.out.println("找吴国太开绿灯");
	}
}