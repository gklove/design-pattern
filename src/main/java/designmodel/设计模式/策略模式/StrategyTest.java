package designmodel.设计模式.策略模式;

public class StrategyTest {
    public static void main(String[] args) {
        //赵云来到吴国，打开第一个锦囊
        System.out.println("-------赵云来到吴国，打开第一个锦囊-----------");
        MyContext context1 = new MyContext(new BackDoor());
        //执行第一个锦囊妙招
        context1.operate();
        System.out.println("-------遇到麻烦打开第二个锦囊-------------");
        //遇到麻烦打开第二个锦囊
        MyContext context2 = new MyContext(new BackDoor());
        //执行第一个锦囊妙招
        context2.operate();
        System.out.println("-------想要溜走，又遇到麻烦---------");
        //想要溜走，又遇到麻烦
        MyContext context3 = new MyContext(new BackDoor());
        //执行第一个锦囊妙招
        context3.operate();
    }
}
