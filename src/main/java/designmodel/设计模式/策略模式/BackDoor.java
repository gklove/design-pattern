package designmodel.设计模式.策略模式;

/**
 * 锦囊妙计一
 * @author admin
 *
 */
public class BackDoor implements IStrategy {
    @Override
    public void operate() {
        System.out.println("找乔国老开后门");
    }
}
