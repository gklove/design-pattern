package designmodel.设计模式.策略模式;

/**
 * 锦囊妙计三
 * @author admin
 *
 */
class BlockEnemy implements IStrategy{
	@Override
	public void operate() {
		System.out.println("孙夫人断后");
	}
}