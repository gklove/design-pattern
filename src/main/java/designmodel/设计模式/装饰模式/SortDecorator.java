package designmodel.设计模式.装饰模式;

public class SortDecorator extends MyDecorator {
    public SortDecorator(SchoolScore schoolScore) {
        super(schoolScore);
    }

    private void getSort() {
        System.out.println("我在班里的排名是：第十...");
    }

    @Override
    public void report() {
        //先报自己的成绩
        super.report();
        //再说明名次
        this.getSort();
    }
}
