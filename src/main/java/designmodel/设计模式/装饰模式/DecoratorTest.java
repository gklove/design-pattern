package designmodel.设计模式.装饰模式;


/**
 * 老师把我的成绩单发下来，让家长签字。因为成绩考的不好，所以不能直接把成绩单拿给爸爸看，
 * 因此我得在拿出成绩单前加一些修饰语。例如：告诉爸爸班里同学的最高分，我的分数是多少。
 * 之后再说我在班里的名次。这样就能避免一场海骂了。
 */
public class DecoratorTest {
    public static void main(String[] args) {
        //拿着自己的原始成绩单
        SchoolScore score = new ConcreteStudent();
        //加一层装饰，最高分的装饰
        score= new HighScoreDecorator(score);
        //再加上第二次修饰，我的排名修饰
        score = new SortDecorator(score);
        //做出报告说明
        score.report();
        //家长签字
        score.sign("ll");
    }
}
