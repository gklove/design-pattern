package designmodel.设计模式.装饰模式;

/**
 * 因为怕父亲看到成绩被他骂，所以我要来个装饰类。
 */
abstract class MyDecorator extends SchoolScore{

    //定义学生的成绩
    private SchoolScore schoolScore;
    //用构造函数，实例化学生成绩类

    public MyDecorator(SchoolScore schoolScore) {
        this.schoolScore = schoolScore;
    }

    //抽象方法，为学生提交成绩单时，添加一些修饰方法。
    @Override
    public void report() {
        this.schoolScore.report();
    }

    //重写父类的方法。让家长签字
    @Override
    public void sign(String name) {
        this.schoolScore.sign(name);
    }
}
