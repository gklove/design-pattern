package designmodel.设计模式.装饰模式;

/**
 * 首先添加一个全班最高成绩的说明，
 * 让父亲知道我这个成绩还算可以。
 * @author admin
 */
public class HighScoreDecorator extends MyDecorator {
    public HighScoreDecorator(SchoolScore schoolScore) {
        super(schoolScore);
    }

    private void highScoreSchool() {
        System.out.println("语文最高分：99,数学最高分：88，英语最高分：77");
    }

    @Override
    public void report() {
        //先说明最高成绩
        this.highScoreSchool();
        //再报自己的成绩
        super.report();
    }
}
