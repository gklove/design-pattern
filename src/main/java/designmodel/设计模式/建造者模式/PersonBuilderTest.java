package designmodel.设计模式.建造者模式;

public class PersonBuilderTest {
    public static void main(String[] args) {
        Person p = new Person.Builder("张三", "男")
                .age(20)
                .car("奔驰")
                .money("200W").build();
        System.out.println(p);
    }
}
