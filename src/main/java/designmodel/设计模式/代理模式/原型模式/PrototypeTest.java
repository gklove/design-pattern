package designmodel.设计模式.代理模式.原型模式;

/**
 * 定义:用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。
 * 原型模式的核心就是一个clone方法，通过该方法进行对象的拷贝，java提供了一个Cloneable接口，来标示这个对象是可拷贝的。
 * 原型模型优点:
 * 性能优良，---原型模型是在内存二进制流的拷贝，比直接new一个对象性能好很多，特别是在循环体内产生大量的对象时，原型模式可以更好的体现其优点。
 * 逃避构造函数的约束，--直接从内存中拷贝，构造函数是不会执行的。
 * 原型模式的注意事项
 * 1、构造函数不会被执行。
 * 2、浅拷贝和深拷贝
 * 浅拷贝
 * --|Object类提供的方法clone只是拷贝本对象，其对象内部的数组、引用对象等都不拷贝，还是指向原声对象的内部元素地址，这种拷贝叫做浅拷贝。
 * 深拷贝
 * --|两个对象之间没有任何瓜葛，你修改你的，我修改我的。互不影响，这种拷贝叫做深拷贝。
 * 3、clone和final是冤家。
 * 出现final的对象或变量，不能clone。
 */
public class PrototypeTest {
    public static void main(String[] args) throws CloneNotSupportedException {

        Myclone myClone = new Myclone();
        myClone.setList("aaa");
        //克隆
        Myclone clone2 =(Myclone) myClone.clone();
        clone2.setList("bbb");
        System.out.println("原对象...."+myClone.getList());
        //输出拷贝后的结果。
        System.out.println("拷贝结果...."+clone2.getList());
        /**
         * 浅层拷贝结果
         * 原对象....[aaa, bbb]
         * 拷贝结果....[aaa, bbb]
         */
        /**
         * 深层拷贝结果
         * 原对象....[aaa]
         * 拷贝结果....[aaa, bbb]
         */
    }
}
