package designmodel.设计模式.代理模式.原型模式;

import javax.jws.Oneway;
import java.util.ArrayList;

public class Myclone implements Cloneable {
    private ArrayList<String> list = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Myclone myclone = null;
        try {
            myclone = (Myclone) super.clone();
            //把私有对象也进行拷贝
            myclone.list = (ArrayList<String>) this.list.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return myclone;
    }

    public ArrayList<String> getList() {
        return list;
    }

    public void setList(String name) {
        this.list.add(name);
    }
}
