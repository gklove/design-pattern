package designmodel.设计模式.代理模式.普通代理;

public class CommonProxyTest {
    public static void main(String[] args) {
        String name = "xy";
        GamePlayerProxy proxy = new GamePlayerProxy(name);
        proxy.login(name, "1234");
        proxy.killBoss();
        proxy.upGrade();
    }
}
