package designmodel.设计模式.代理模式.普通代理;

interface IGamePlayer {
    void login(String name, String password);

    void killBoss();

    void upGrade();
}
