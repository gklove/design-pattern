package designmodel.设计模式.代理模式.普通代理;

public class GamePlayerProxy implements IGamePlayer {
    private GamePlayer gamePlayer=null;

    //通过构造函数来传递要对谁进行游戏代理
    public GamePlayerProxy(String name) {
        try {
            this.gamePlayer = new GamePlayer(this, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void login(String name, String password) {
        this.gamePlayer.login(name,password);
    }

    @Override
    public void killBoss() {
        this.gamePlayer.killBoss();
    }

    @Override
    public void upGrade() {
        this.gamePlayer.upGrade();
    }
}
