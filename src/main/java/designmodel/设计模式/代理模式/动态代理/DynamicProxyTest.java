package designmodel.设计模式.代理模式.动态代理;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * 定义:为其他对象提供一种代理以控制对这个对象的访问。
 *
 * 我的理解:代理模式相当于现实生活中的明星经纪人，提供了一个很好的访问控制。
 *
 * 代理模式的分类:
 *
 * 静态代理模式
 * --|普通代理模式
 * --|强制代理模式
 *
 * 动态代理模式
 */
public class DynamicProxyTest {
    public static void main(String[] args) {
        String name = "yy";
        String password = "1234";
        //定义一个游戏玩家
        MyIGamePlayer player = new MyGamePlayer(name);
        //定义一个handler
        InvocationHandler handler = new GamePlayInvocationHandler(player);
        //开始游戏，获得类的classLoader
        ClassLoader classLoader = player.getClass().getClassLoader();
        //动态生成一个代理者
        MyIGamePlayer proxy = (MyIGamePlayer) Proxy.newProxyInstance(classLoader, new Class[]{MyIGamePlayer.class}, handler);

        proxy.login(name, password);
        proxy.killBoss();
        proxy.upGrade();
    }
}
