package designmodel.设计模式.代理模式.动态代理;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class GamePlayInvocationHandler implements InvocationHandler {
    //设置动态代理，必须继承InvocationHandler接口。并实现invoker方法
    //被代理者
    Class cls = null;
    Object obj = null;

    public GamePlayInvocationHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(this.obj, args);
        if (method.getName().equalsIgnoreCase("killBoss")) {
            System.out.println("有人用我的账号登录...");
        }
        return result;
    }
}
