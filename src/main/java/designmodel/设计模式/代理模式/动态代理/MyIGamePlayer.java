package designmodel.设计模式.代理模式.动态代理;

public interface MyIGamePlayer {
    void login(String name, String password);

    void killBoss();

    void upGrade();
}
