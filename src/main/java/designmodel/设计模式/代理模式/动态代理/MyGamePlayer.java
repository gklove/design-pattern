package designmodel.设计模式.代理模式.动态代理;

public class MyGamePlayer implements MyIGamePlayer {
    private String name = "";

    //设置一个提供代理类的构造函数，限制用户必须通过代理类来实例化对象。
    public MyGamePlayer(String name) {
        this.name = name;
    }
    @Override
    public void login(String name, String password) {
        System.out.println("用户："+this.name+"登陆成功！");
    }

    @Override
    public void killBoss() {
        System.out.println(this.name+"在打怪！");
    }

    @Override
    public void upGrade() {
        System.out.println(this.name+"又升了一级....");
    }
}
