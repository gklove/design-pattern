package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

public class MySubject implements ISubject {
    @Override
    public void doSomething() {
        System.out.println("子类的具体实现方法....");
    }
}
