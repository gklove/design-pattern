package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

public class MyAdvice implements IAdvice {
    @Override
    public void someAdvice() {
        System.out.println("一些前言方法....");
    }
}
