package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class DynamicProxys {
    @SuppressWarnings("unchecked")
    public static <T> T newProxyInstance(ClassLoader classLoader, Class<?>[] interfaces, InvocationHandler handler) {
        if (true) {
            //执行一个前置通知
            new MyAdvice().someAdvice();

        }
        return (T) Proxy.newProxyInstance(classLoader, interfaces, handler);
    }
}
