package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

public interface ISubject {
    void doSomething();
}
