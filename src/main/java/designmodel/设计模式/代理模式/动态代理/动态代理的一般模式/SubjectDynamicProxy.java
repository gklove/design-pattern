package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class SubjectDynamicProxy {
    public static <T> T newProxyInstance(ISubject subject) {
        //获取classloader
        ClassLoader classLoader = subject.getClass().getClassLoader();
        //获得数组接口，
        Class<?>[] interfaces = subject.getClass().getInterfaces();
        //获得handler
        InvocationHandler handler = new MyInvocationHandler(subject);
        return (T) Proxy.newProxyInstance(classLoader, interfaces, handler);
    }
}
