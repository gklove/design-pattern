package designmodel.设计模式.代理模式.动态代理.动态代理的一般模式;

public class DynamicCommonProxy {
    public static void main(String[] args) {
        //定义一个主题
        ISubject subject = new MySubject();
        //定义主题代理
        ISubject proxy = SubjectDynamicProxy.newProxyInstance(subject);
        //也可以将上面的改为SubjectDynamicProxy改为DynamicProxys
        proxy.doSomething();
    }
}
