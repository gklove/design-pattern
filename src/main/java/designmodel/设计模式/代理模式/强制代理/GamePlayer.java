package designmodel.设计模式.代理模式.强制代理;

public class GamePlayer implements IGamePlayer {
    private String name = "";
    private IGamePlayer proxy = null;

    //设置一个提供代理类的构造函数，限制用户必须通过代理类来实例化对象。
    public GamePlayer(String name) {
        this.name = name;
    }
    @Override
    public void login(String name, String password) {
        if (this.isProxy()) {
            System.out.println("用户：" + this.name + "登录成功！");
        } else {
            System.out.println("请使用指定的代理访问...");
        }
    }

    @Override
    public void killBoss() {
        if(this.isProxy()){
            System.out.println(this.name+"在打怪！");
        }
        else{
            System.out.println("请使用指定的代理访问...");
        }
    }

    @Override
    public void upGrade() {
        if(this.isProxy()){
            System.out.println(this.name+"又升了一级!");
        }
        else{
            System.out.println("请使用指定的代理访问...");
        }
    }

    @Override
    public IGamePlayer getProxy() {
        this.proxy = new GameProxy(this);
        return this.proxy;
    }

    private boolean isProxy() {
        if (proxy == null) {
            return false;
        }
        return true;
    }
}
