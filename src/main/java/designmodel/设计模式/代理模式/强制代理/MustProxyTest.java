package designmodel.设计模式.代理模式.强制代理;

public class MustProxyTest {
    public static void main(String[] args) {
        //首先创建一个游戏玩家
        System.out.println("------只创建一个玩家，不设置代理的效果---------");
        String name = "yy";
        GamePlayer yy = new GamePlayer(name);
        yy.login(name, "pass");
        yy.killBoss();
        yy.upGrade();
        System.out.println("------创建一个玩家，并设置代理的效果---------");
        GameProxy proxy = (GameProxy)yy.getProxy();
        proxy.login(name, "pass");
        proxy.killBoss();
        proxy.upGrade();
    }
}
