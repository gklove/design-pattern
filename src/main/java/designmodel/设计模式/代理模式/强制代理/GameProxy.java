package designmodel.设计模式.代理模式.强制代理;

public class GameProxy implements IGamePlayer {
    private IGamePlayer gamePlayer = null;

    //通过构造函数来传递要对谁进行游戏代理
    public GameProxy(IGamePlayer gamePlayer) {
        this.gamePlayer = gamePlayer;
    }
    @Override
    public void login(String name, String password) {
        this.gamePlayer.login(name, password);
    }

    @Override
    public void killBoss() {
        this.gamePlayer.killBoss();
    }

    @Override
    public void upGrade() {
        this.gamePlayer.upGrade();
    }

    //设置代理，代理没有具体的实例，因此暂时返回自己。
    @Override
    public IGamePlayer getProxy() {
        return this;
    }
}
