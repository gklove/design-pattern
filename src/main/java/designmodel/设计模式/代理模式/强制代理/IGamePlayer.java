package designmodel.设计模式.代理模式.强制代理;

public interface IGamePlayer {
    void login(String name, String password);

    void killBoss();

    void upGrade();

    IGamePlayer getProxy();
}
