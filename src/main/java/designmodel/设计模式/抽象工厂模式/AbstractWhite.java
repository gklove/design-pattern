package designmodel.设计模式.抽象工厂模式;

abstract class AbstractWhite implements Human{
    @Override
    public void getSkin() {
        System.out.println("我是白种人,拥有白色的皮肤");
    }

    @Override
    public void talk() {
        System.out.println("我是白种人，说的是英语");
    }
}
