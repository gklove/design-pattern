package designmodel.设计模式.抽象工厂模式;

public interface Human {
    //肤色
    void getSkin();

    //说话语言
    void talk();

    //性别
    void getSex();
}
