package designmodel.设计模式.抽象工厂模式;

interface HumanFactory{
	//创建黑人
	Human createBlackHuman();
	//创建黄种人
	Human createYellowHuman();
	//创建白人
	Human createWhiteHuman();
}