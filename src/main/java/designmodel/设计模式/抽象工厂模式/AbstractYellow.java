package designmodel.设计模式.抽象工厂模式;

abstract class AbstractYellow implements Human {
    @Override
    public void getSkin() {
        System.out.println("我的黄种人，拥有黄色的皮肤");
    }

    @Override
    public void talk() {
        System.out.println("我的黄种人，说的是汉语");
    }
}
