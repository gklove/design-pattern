package designmodel.设计模式.抽象工厂模式;

abstract class AbstractBlack implements Human {
    @Override
    public void getSkin() {
        System.out.println("我是黑人,拥有黑色肤色");
    }

    @Override
    public void talk() {
        System.out.println("我是黑人,讲的是非洲语");
    }
}
