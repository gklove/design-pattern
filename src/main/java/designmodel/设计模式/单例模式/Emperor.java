package designmodel.设计模式.单例模式;

//通过单例的拓展，让类只产生两个或者多个实例对象。

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Emperor {
    private static List<Emperor> emperorList = new ArrayList<>();
    private static int maxNum = 2;
    //定义一个集合，为对象设置编号
    private static List<String> listNumOfEmperor = new ArrayList<>();
    //表示当前编号
    private static int currentNum = 0;

    //创建出所有的对象
    static {
        for (int i = 0; i < maxNum; i++) {
            emperorList.add(new Emperor("实例化..."+ (i + 1) + "...对象")) ;
        }
    }

    private Emperor(String name) {
        listNumOfEmperor.add(name);
    }

    public static Emperor getEmperor() {
        Random random = new Random();
        currentNum = random.nextInt(maxNum);
        return emperorList.get(currentNum);
    }

    public void say() {
        System.out.println(listNumOfEmperor.get(currentNum));
    }
}
