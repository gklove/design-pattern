package designmodel.设计模式.单例模式;

//懒汉式单例模式在遇到高并发情况，系统有压力增大，可能会出现多个实例的对象
//懒汉式单例模式

/**
 * 单例模式的优点：
 *
 * 1、单例模式内存中只有一个实例，减少内存开支，一个对象需要频繁的创建和销毁。使用单例模式具有很大的优势。
 *
 * 2、单例只生成一个实例，减少系统的性能开销。
 *
 * 3、单例模式可以在避免对资源的多重占用。
 *
 * 4、单例模式可以在系统设置全局的访问点，优化和共享资源访问。
 *
 * 单例模式的缺点：
 *
 * 1、单例模式一般没有接口，扩展很困难。
 * 2、单例对测试不利
 * 3、单例与单一职责原则有冲突。
 *
 * 单例的使用场景:
 *
 * 1、要求生成唯一序列号的环境
 * 2、在整个项目中需要一个共享访问点或访问数据
 * 3、创建一个对象需要消耗的资源过多
 * 4、需要定义大量的静态常量和静态方法。
 */
public class Singleton {
    private static Singleton single = null;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (single == null) {
            synchronized (Singleton.class) {
                if (single == null) {
                    single = new Singleton();
                }
            }
        }
        return single;
    }
}
