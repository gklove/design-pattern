package designmodel.设计模式.单例模式;

public class EmperorTest {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            //获取对象
            Emperor emperor = Emperor.getEmperor();
            emperor.say();
        }
    }
}
