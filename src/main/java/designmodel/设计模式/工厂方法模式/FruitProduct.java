package designmodel.设计模式.工厂方法模式;

public class FruitProduct extends Product {
    @Override
    public void method2() {
        System.out.println("水果工厂，生产出来了果汁....");
    }
}
