package designmodel.设计模式.工厂方法模式;

/**
 * 工厂模式的优点:
 *
 * 1、具有良好的封装性，代码结构清晰。降低模块间的耦合性
 *
 * 2、工厂方法模式的扩展性非常优秀。
 *
 * 3、屏蔽产品类，产品类对外提供的是一个接口，只要接口不改变系统中的上层模块就不会发生改变。
 */
public class FactoryTest {
    public static void main(String[] args) {
        //通过创建子类对象，将接口实例化。
        Creator cFactory = new NumberOneCreator();
        FoodProduct food = cFactory.createProduct(FoodProduct.class);
        FruitProduct fruit = cFactory.createProduct(FruitProduct.class);
        food.method2();
        food.method1();
        fruit.method2();

    }
}
