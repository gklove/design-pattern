package designmodel.设计模式.工厂方法模式;

abstract class Product {
    public void method1() {
        System.out.println("方法一，加工的都是使用的产品....");
    }

    public abstract void method2();
}
