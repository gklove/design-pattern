package designmodel.设计模式.工厂方法模式;

public class FoodProduct extends Product {
    @Override
    public void method2() {
        System.out.println("食物工厂，生产出来了零食....");
    }
}
