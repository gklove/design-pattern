package designmodel.设计模式.工厂方法模式;

public class NumberOneCreator extends Creator {
    @Override
    public <T extends Product> T createProduct(Class<T> t) {
        Product p = null;
        try {
            //Class类创建一个 工厂类实例
            p=   (Product)Class.forName(t.getName()).newInstance();
        } catch (Exception  e) {
            e.printStackTrace();
        }
        return (T)p;
    }
}
