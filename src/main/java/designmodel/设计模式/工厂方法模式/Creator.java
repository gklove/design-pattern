package designmodel.设计模式.工厂方法模式;

abstract class Creator {
    public abstract <T extends Product> T createProduct(Class<T> t);


}
