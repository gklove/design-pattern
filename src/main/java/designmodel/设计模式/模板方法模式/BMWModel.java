package designmodel.设计模式.模板方法模式;

public class BMWModel extends Model {

    private boolean flag = true;

    public void setAlarm(boolean flag) {
        this.flag = flag;
    }

    protected boolean isAlarm() {
        return flag;
    }
    @Override
    protected void start() {
        System.out.println("宝马车启动...");
    }

    @Override
    protected void engineBoom() {
        System.out.println("宝马车发动机轰鸣...");
    }

    @Override
    protected void alarm() {
        System.out.println("宝马车开始鸣笛...");
    }

    @Override
    protected void stop() {
        System.out.println("宝马车停止...");
    }
}
