package designmodel.设计模式.模板方法模式;

abstract class Model {
    protected abstract void start();
    protected abstract void engineBoom();
    protected abstract void alarm();
    protected abstract void stop();

    protected void run() {
        this.start();
        this.engineBoom();
        if (isAlarm()) {
            this.alarm();
        }
        this.stop();
    }

    protected boolean isAlarm() {
        return true;
    }
}
