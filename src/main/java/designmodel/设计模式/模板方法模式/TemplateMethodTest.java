package designmodel.设计模式.模板方法模式;

public class TemplateMethodTest {
    public static void main(String[] args) {
        System.out.println("------奔驰车模板----------");
        System.out.println("用户设置车辆是否鸣笛，1表示可以鸣笛，2表示不可以鸣笛.");
        BMWModel bmwModel = new BMWModel();
        bmwModel.setAlarm(true);
        bmwModel.run();
    }
}
