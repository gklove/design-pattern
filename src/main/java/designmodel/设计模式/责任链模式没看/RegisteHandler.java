package designmodel.设计模式.责任链模式没看;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

abstract class RegisteHandler{
	//VIP等级用户注册
	public static final int VIP_LEVEL = 2;
	//普通用户注册
	public static final int COMMON_LEVEL = 1;
	//设置hashMap存储登录信息
	private HashMap<String, Integer> infoMap = new HashMap<String, Integer>();
	//定义当前的等级
	private int level ;
	//定义下一个责任链
	private RegisteHandler nextHandler;
	//构造方法，设置责任人等级。
	public RegisteHandler(int level){
		this.level = level;
	}
	//处理信息
	public void HandleMessage(HashMap<String, Integer> hashInfo){
		//遍历hash表中的所用信息。
		Set<String> set = hashInfo.keySet();
		Iterator<String> it = set.iterator();
		while(it.hasNext()){
			String name = it.next();
			//通过name获得 他的注册类型。
			int type = hashInfo.get(name);
			if(this.level == type){
				this.infoMap.clear();
				this.infoMap.put(name, type);
				System.out.println("---------普通用户--------------");
				//如果当前类型与注册类型相同，则执行方法。
				this.response(infoMap);
			}else{
				//如果没有找到责任人，继续查找
				if(nextHandler != null){
					this.infoMap.clear();
					this.infoMap.put(name, type);
					System.out.println("---------VIP用户--------------");
					this.nextHandler.response(infoMap);
				}else{
					//如果找不到责任人。
					System.out.println("没有您选择的注册类型，请重新注册....");
				}
			}
		}
	}
	public void setHandler(RegisteHandler handler){
		this.nextHandler = handler;
	}
	//定义请求的信息。
	protected abstract void response(HashMap<String, Integer> hashMap);
}