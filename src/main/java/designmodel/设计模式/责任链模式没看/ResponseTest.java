package designmodel.设计模式.责任链模式没看;

import java.util.HashMap;

public class ResponseTest {
	public static void main(String[] args) {
		//定义一些登录者
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		hashMap.put("lzl", 1);
		hashMap.put("xy", 2);
		hashMap.put("ht", 2);
		hashMap.put("hj", 2);
		hashMap.put("zl",1);
		//创建登录
		Registe register = new Registe();
		register.setHashMap(hashMap);
		//通过Handler处理登录信息,并通过责任链入口、
		RegisteHandler CommonHandler = new CommonRegiste();
		RegisteHandler VIPHandler = new VIPRegiste();
		//设置下一个责任人..
		CommonHandler.setHandler(VIPHandler);
		CommonHandler.HandleMessage(hashMap);
		
	}
}