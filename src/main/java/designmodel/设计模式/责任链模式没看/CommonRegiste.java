package designmodel.设计模式.责任链模式没看;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class CommonRegiste extends RegisteHandler{
	//构造函数设置普通用户的等级。
	public CommonRegiste() {
			super(RegisteHandler.COMMON_LEVEL);
		}
	
	@Override
	protected void response(HashMap<String, Integer> hashMap) {
		Set<Map.Entry<String, Integer>> set = hashMap.entrySet();
		Iterator<Map.Entry<String, Integer>> it = set.iterator();
		while(it.hasNext()){
			Map.Entry<String, Integer> map = it.next();
			String name = map.getKey();
			System.out.println("普通用户:"+name+"\t注册成功！");
		}
	}
}