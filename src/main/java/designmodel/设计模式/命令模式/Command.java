package designmodel.设计模式.命令模式;

abstract class Command {
    public abstract void execute();
}
