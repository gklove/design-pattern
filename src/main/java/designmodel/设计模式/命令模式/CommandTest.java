package designmodel.设计模式.命令模式;

/**
 * 定义:将一个请求封装成一个对象，从而让你使用不同的请求把客户端参数化，对请求排队或者记录请求日志，
 *
 * 可以提供命令的撤销和恢复功能。
 */
public class CommandTest {
    public static void main(String[] args) {
        //首先声明调用者Invoker
        MyInvoker myInvoker = new MyInvoker();
        //声明命令的接收者
        Receiver receiver1 = new ConcreteReceive1();
        //声明命令
        Command command1 = new ConcreteCommand1(receiver1);
        myInvoker.setCommand(command1);
        myInvoker.action();
    }
}
