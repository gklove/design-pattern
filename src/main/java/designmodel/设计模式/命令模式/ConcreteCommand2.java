package designmodel.设计模式.命令模式;

public class ConcreteCommand2 extends Command {
    private Receiver receiver;
    //用构造函数来传递具体接受者是谁
    public ConcreteCommand2(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.doSomething();
    }
}
