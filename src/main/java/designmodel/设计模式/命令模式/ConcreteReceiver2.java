package designmodel.设计模式.命令模式;

public class ConcreteReceiver2 extends Receiver {
    @Override
    public void doSomething() {
        System.out.println("接收者2号,需要执行的方法...");
    }
}
