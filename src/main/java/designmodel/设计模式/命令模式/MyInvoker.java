package designmodel.设计模式.命令模式;

public class MyInvoker {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    //调用者角色，去决定具体的接收者执行什么命令
    public void action() {
        this.command.execute();

    }
}
