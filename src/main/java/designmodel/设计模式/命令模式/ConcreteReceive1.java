package designmodel.设计模式.命令模式;

public class ConcreteReceive1 extends Receiver {
    @Override
    public void doSomething() {
        System.out.println("接受者1号，需要执行的方法....");
    }
}
