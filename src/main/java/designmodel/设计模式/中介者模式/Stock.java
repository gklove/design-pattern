package designmodel.设计模式.中介者模式;

//库存部门
public class Stock  extends AbstractColleague {

    //库存默认有10台电脑
    private static int CURRENT_NUMBER = 10;
    public Stock(AbstractMediator abstractMediator) {
        super(abstractMediator);
    }

    //增加库存
    public void increase(int number) {
        CURRENT_NUMBER += number;
    }

    //获得库存
    public int getStock() {
        return CURRENT_NUMBER;
    }

    //减少库存
    public void decrease(int number) {
        CURRENT_NUMBER -= number;
    }

    //清空库存
    public void clearStock() {
        System.out.println("清空了仓库里的" + CURRENT_NUMBER + "台电脑");
        CURRENT_NUMBER = 0;
    }


}
