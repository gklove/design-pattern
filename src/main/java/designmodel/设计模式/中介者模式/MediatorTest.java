package designmodel.设计模式.中介者模式;

/**
 *
 * 代码写的有问题
 * 一个公司有采购部门、销售部门、存货部门。他们之间存在着紧密的联系。
 * 采购部门(Purchase)：根据销售情况、库存情况，负责采购不同的产品。
 *     --|void buyIBMComputer(int number)
 *     --|void refuseBuyIBM();
 * 销售部门(sale)：销售部门要反馈销售情况，畅销就多采购，滞销就不采购。
 *    --|void sellIBMComputer(int number);
 *    --|int getSaleStatus();
 *    --|void offSale();
 * 存货部门(Stock)：根据销售情况，已经自身库存的数量，决定是否需要采购。
 *    --|void increase(int number)
 *    --|void decrease(int number)
 *    --|int getStockNumber()
 *    --|void clearStock();
 * 中介者模式的优点：
 * 减少类间的依赖，把原有的一堆多的依赖变成了一对一的依赖，同事类只依赖中介者，减少了依赖。同时降低了类间的耦合。
 */
public class MediatorTest {
    public static void main(String[] args) {

        //创建中介者
        AbstractMediator mediator = new ComMediator();
        //销售者进行销售,100台电脑
        Sale sale = new Sale(mediator);
        sale.sellComputer(100);
        //获取库存状态
        Stock stock = new Stock(mediator);
        System.out.println("库存状态.."+stock.getStock());
        //采购者进行采购
        Purchase purchase = new Purchase(mediator);
//        purchase.BuyComputer(1000);
        //获取库存状态
        System.out.println("库存状态.."+stock.getStock());
    }
}
