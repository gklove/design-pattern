package designmodel.设计模式.中介者模式;

public class Purchase extends AbstractColleague {
    public Purchase(AbstractMediator abstractMediator) {
        super(abstractMediator);
    }

    public void BuyComputer(int number) {
        super.abstractMediator.execute("purchase.buy",number);
    }

    public void refuseBuyComputer() {
        System.out.println("拒绝购买电脑....");
    }
}
