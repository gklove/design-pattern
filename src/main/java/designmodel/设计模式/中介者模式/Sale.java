package designmodel.设计模式.中介者模式;

import java.util.Random;

public class Sale  extends AbstractColleague{
    public Sale(AbstractMediator abstractMediator) {
        super(abstractMediator);
    }

    //销售电脑
    public void sellComputer(int number) {
        //通过中介者销售电脑
        super.abstractMediator.execute("sale.sell", number);
    }

    //获得销售状态
    public int getSaleStatus() {
        //随机生成数字 表示销售状态
        Random random = new Random();
        int saleStatus = random.nextInt(100);
        return saleStatus;
    }

    //打折销售
    public void offSale() {
        //通过中介者打折
        super.abstractMediator.execute("sale.offSale");
    }

}
