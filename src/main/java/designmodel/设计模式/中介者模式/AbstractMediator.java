package designmodel.设计模式.中介者模式;

abstract class AbstractMediator {
    //库存部门
    protected Stock stock = null;
    //销售部门
    protected Sale sale = null;
    //采购部门
    protected Purchase purchase = null;

    public AbstractMediator() {
        this.sale = new Sale(this);
        this.stock = new Stock(this);
        this.purchase = new Purchase(this);
    }

    //定义一个中介者执行方法。共子类实现
    public abstract void execute(String type, Object... objects);
}
