package designmodel.设计模式.中介者模式;

public class ComMediator extends AbstractMediator {
    @Override
    public void execute(String type, Object... objects) {
        if (type.equalsIgnoreCase(Constant.purchase)) {
            this.buyComputer((Integer) objects[0]);
        } else if (type.equalsIgnoreCase(Constant.sale)) {
            saleComputer((Integer)objects[0]);
        } else if (type.equalsIgnoreCase(Constant.saleOff)) {
            offSell();
        } else if (type.equalsIgnoreCase(Constant.stockClear)) {
            clearStore();
        }
    }

    //清空仓库
    private void clearStore() {
        //清空所有电脑
        super.stock.clearStock();
    }

    //购买电脑
    private void buyComputer(int number) {
        //首先获取销售状态  如果销售状态大于 80 就可以进行采购
        int saleStatus = super.sale.getSaleStatus();
        System.out.println("销售状态是："+saleStatus);
        if (saleStatus > 80) {
            System.out.println("销售状态优秀，正常采购");
            super.purchase.BuyComputer(number);
            //购买完电脑 一定要增加库存
            super.stock.increase(number);
        } else {
            //效益不好，折半采购
            System.out.println("销售效益不好，折半采购");
            int buyNumber = number / 2;
            super.purchase.BuyComputer(number);
            super.stock.increase(number);
        }
    }

    //销售电脑
    private void saleComputer(int number) {
        //检查库存数量
        if (super.stock.getStock() < number) {
            //库存量不足，通知采购电脑
            System.out.println("存量不足，通知采购电脑");
            super.purchase.BuyComputer(number);
        } else {
            //库存减去相应的电脑数量
            super.stock.decrease(number);

        }
    }

    //折半销售电脑
    private void offSell() {
        System.out.println("折半销售电脑："+super.stock.getStock()+"台");
    }


}
